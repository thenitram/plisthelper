/*
 * Copyright (C) 2015  John Martin Magalong
 * PlistHelper is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * PlistHelper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

#import <Foundation/Foundation.h>

@interface PlistHelper : NSObject


+ (id)read:(NSString *)fileName;

/**
Creates or returns an NSDictionary/NSArray.
@param fileName Filename of the plist without the .plist extension.
@returns Loosely-typed plist data
*/
+ (NSDictionary *)getWritableDictionaryPlist:(NSString*) filenameNoExt;
+ (NSArray *)getWritableArrayPlist:(NSString*) filenameNoExt;

/**
 Writes an array/dictionary to plist
 @param plist Array/Dictionary
 @param fileName Filename of the plist
 */
+ (void)write:(id)data fileName:(NSString *)fileName;
+ (void)saveWritablePlist:(NSString*) filenameNoExt data:(id) data;
@end
