/*
* Copyright (C) 2015  John Martin Magalong
* PlistHelper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* PlistHelper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
* @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
*/

#import "PlistHelper.h"

@implementation PlistHelper

+ (id)read:(NSString *)fileName {
    NSData *plistData;
    NSString *error;
    NSPropertyListFormat format;
    id plist;
    
    NSString *localizedPath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    plistData = [NSData dataWithContentsOfFile:localizedPath];
    
    plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&format error:nil];

    if (!plist) {
        NSLog(@"Error reading plist from file '%s', error = '%s'", [localizedPath UTF8String], [error UTF8String]);
    }
    
    return plist;
}

+ (void)write:(id)data fileName:(NSString *)fileName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryPath = [paths objectAtIndex:0];
    NSString *cachePath = [libraryPath stringByAppendingPathComponent:@"/Caches/Plist"];
    cachePath = [cachePath stringByAppendingFormat:@"/%@.plist",fileName];
    [(NSDictionary *)data writeToFile:cachePath atomically:YES];
}

+ (NSDictionary *)getWritableDictionaryPlist:(NSString*) filenameNoExt{
    //get file paths
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",filenameNoExt]];
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];  
    NSString *bundlePlistPath = [bundlePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",filenameNoExt]];
    
    //if file exists in the documents directory, get it
    if([fileManager fileExistsAtPath:documentPlistPath]){            
        NSMutableDictionary *documentDict = [NSMutableDictionary dictionaryWithContentsOfFile:documentPlistPath];
        return documentDict;
    } 
    //if file does not exist, create it from existing plist
    else {
        NSError *error;
        BOOL success = [fileManager copyItemAtPath:bundlePlistPath toPath:documentPlistPath error:&error];
        if (success) {
            NSMutableDictionary *documentDict = [NSMutableDictionary dictionaryWithContentsOfFile:documentPlistPath];
            return documentDict;
        }
        return nil;
    }
}

+ (NSArray *)getWritableArrayPlist:(NSString*) filenameNoExt{
    //get file paths
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",filenameNoExt]];
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    NSString *bundlePlistPath = [bundlePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",filenameNoExt]];
    
    //if file exists in the documents directory, get it
    if([fileManager fileExistsAtPath:documentPlistPath]){
        NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
        return documentDict;
    }
    //if file does not exist, create it from existing plist
    else {
        NSError *error;
        BOOL success = [fileManager copyItemAtPath:bundlePlistPath toPath:documentPlistPath error:&error];
        if (success) {
            NSMutableArray *documentDict = [NSMutableArray arrayWithContentsOfFile:documentPlistPath];
            return documentDict;
        }
        return nil;
    }
}

+ (void)saveWritablePlist:(NSString*) filenameNoExt data:(id) data{
    //get file paths
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentPlistPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",filenameNoExt]];
   
    if([data isKindOfClass:[NSDictionary class]]){
        [(NSDictionary *)data writeToFile:documentPlistPath atomically:YES];
    }
    else if([data isKindOfClass:[NSArray class]]){
        [(NSArray *)data writeToFile:documentPlistPath atomically:YES];
    }
}

@end
